import javax.management.ObjectName;

public class Factorial {

    public void getValueFactorial(int factorialNumber) {
        if (!checkingConditions(factorialNumber))
            return;
        int res = getRecursionFactorial(factorialNumber);
        System.out.println("Факториал числа " + factorialNumber + " через рекурсию равен: " + res);
        res = getSimpleFactorial(factorialNumber);
        System.out.println("Факториал числа " + factorialNumber + " через цикл равен: " + res);
    }

    private boolean checkingConditions(int value) {
        boolean resultCheck = true;
        if (value < 0) {
            resultCheck = false;
            System.out.println("Число не должно быть отрицательным");
        } else if (value == 0) {
            resultCheck = false;
            System.out.println("Число не должно быть равно нулю");
        }
        return resultCheck;
    }

    private int getRecursionFactorial(int factorialNumber) {
        if (factorialNumber <= 1) {
            return 1;
        } else {
            return factorialNumber * getRecursionFactorial(factorialNumber - 1);
        }
    }

    private int getSimpleFactorial(int factorialNumber) {
        int result = 1;
        for (int i = 1; i <= factorialNumber; i++) {
            result = result * i;
        }
        return result;
    }

}

